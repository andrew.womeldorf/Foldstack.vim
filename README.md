# Foldstack

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Demo](#demo)
- [Installation](#installation)
- [Usage](#usage)
  - [Hotkey](#hotkey)
- [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

Foldstack adds a command to quickly fold PHP stack traces in a log file. In very large log files, where many errors have been thrown, this can make navigation for a particular error faster and cleaner.

I *think* it wouldn't be too much work to expand this to other languages. If you have recommendations, create an issue!

## Demo

![demo.gif](demo.gif)

## Installation

**Requirements:**

- Vim compiled with Python 3 support

Installation can be done with [Vundle](https://github.com/VundleVim/Vundle.vim):
```
Plugin 'https://gitlab.com/andrew.womeldorf/Foldstack.vim'
```
Then, open Vim and run `:PluginInstall`.

## Usage

From within your log file, call `:Foldstack`.

### Hotkey

For faster folding, I use this keymap:
```
map <leader>f :Foldstack<CR>
```

## License

This plugin is released under the MIT License
