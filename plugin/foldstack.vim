" ============================================================================
" File:        foldstack.vim
" Description: Fold stack traces in log files
" Author:      Andrew Womeldorf <andrew DOT womeldorf AT gmail.com>
" Licence:     This file is placed in the public domain
" Website:     https://gitlab.com/andrew.womeldorf/Foldstack.vim
" Version:     0.0.0.0
" Note:        This plugin is primarily geared toward PHP log files for now.
" ============================================================================

" avoid double loading
if exists('b:is_foldstack_loaded')
    finish
endif

" set special flag to prevent double loading on a buffer
let b:is_foldstack_loaded = 1
"
" Do not source this script when python is not compiled in.
if !has("python3")
    echomsg ":python3 is not available, foldstack will not be loaded."
    finish
endif

" Get the python script
execute 'py3file' fnamemodify(expand('<sfile>'), ':p:h:h') . '/python/folder.py'

" Main worker
python3 folder = Folder()

" Commands
command! -nargs=? Foldstack python3 folder.run()
