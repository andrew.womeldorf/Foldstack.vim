import vim
import re

class Folder:
    def run(self):
        buf_len = len(vim.current.buffer)
        linenum = 1
        end_linenum = buf_len - 1

        trace_start = 0
        tracking = False

        for i in range(linenum, end_linenum):
            buf_line = vim.current.buffer[i]

            # if line doesn't contain "PHP   1. ", where 1 can be any number, then its the end of the fold
            if tracking is True and re.search('PHP\s+\d+\.\s', buf_line) is None:
                tracking = False
                self.fold(trace_start, i)
                continue

            # if line contains "PHP stack trace:", it's one line after the start of the fold
            if tracking is False and re.search('PHP\sStack\strace:', buf_line) is not None:
                trace_start = i
                tracking = True

        # Close the fold if we reach the end of the file and still tracking
        if tracking is True:
            real_end = end_linenum + 1
            self.fold(trace_start, real_end)

    def fold(self, trace_start, trace_end):
        vim.command(f'{trace_start},{trace_end} fold')
